<?php

namespace App\Http\Controllers;

use App\Models\SitePostData;
use Illuminate\Http\Request;

class SitePostDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SitePostData  $sitePostData
     * @return \Illuminate\Http\Response
     */
    public function show(SitePostData $sitePostData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SitePostData  $sitePostData
     * @return \Illuminate\Http\Response
     */
    public function edit(SitePostData $sitePostData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SitePostData  $sitePostData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SitePostData $sitePostData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SitePostData  $sitePostData
     * @return \Illuminate\Http\Response
     */
    public function destroy(SitePostData $sitePostData)
    {
        //
    }
}
