<?php

namespace App\Http\Controllers;

use App\Models\SiteTag;
use Illuminate\Http\Request;

class SiteTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteTag  $siteTag
     * @return \Illuminate\Http\Response
     */
    public function show(SiteTag $siteTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteTag  $siteTag
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteTag $siteTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteTag  $siteTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteTag $siteTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteTag  $siteTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteTag $siteTag)
    {
        //
    }
}
