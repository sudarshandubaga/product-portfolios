@extends('siteadmin.layouts.app')

@section('title', 'Admin Login')

@section('body_content')
<div id="login_wrapper">
    <div>
        <img src="{{ url('handmade/images/logo.png') }}" alt="" srcset="">
    </div>
    <div>
        <form class="login_form" action="{{ route('admin.login.post') }}" method="post">
            @csrf
            <h3>Login</h3>
            <p>Please enter your username and password to login.</p>
            @include('siteadmin.components.flashmessage')
            @include('siteadmin.components.errors')
            <div class="mb-3">
                <input type="text" name="login" id="" placeholder="Username *" autocomplete="off">
            </div>
            <div class="mb-3">
                <input type="password" name="password" id="" placeholder="Password *" autocomplete="new-password">
            </div>
            <div class="mb-3">
                <label>
                    <input type="checkbox" name="" id="">
                    Remember Me?
                </label>
            </div>
            <button type="submit" class="btn btn-full btn-primary">Login</button>
        </form>
    </div>
</div>
@endsection