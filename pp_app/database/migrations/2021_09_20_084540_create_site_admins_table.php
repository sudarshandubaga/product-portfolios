<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_admins', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191)->nullable();
            $table->string('login', 191);
            $table->string('password', 191)->nullable();
            $table->string('api_token', 191)->nullable();
            $table->string('mobile_no', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->unsignedBigInteger('site_id');
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->unique(['login', 'site_id']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_admins');
    }
}
