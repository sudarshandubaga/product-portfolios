<?php

namespace App\Http\Controllers;

use App\Models\SiteProductData;
use Illuminate\Http\Request;

class SiteProductDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteProductData  $siteProductData
     * @return \Illuminate\Http\Response
     */
    public function show(SiteProductData $siteProductData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteProductData  $siteProductData
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteProductData $siteProductData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteProductData  $siteProductData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteProductData $siteProductData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteProductData  $siteProductData
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteProductData $siteProductData)
    {
        //
    }
}
