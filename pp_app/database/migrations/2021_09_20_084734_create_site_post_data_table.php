<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitePostDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_post_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('site_post_id');
            $table->foreign('site_post_id')->references('id')->on('site_posts')->onDelete('cascade');
            $table->string('attribute', 191);
            $table->string('value', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_post_data');
    }
}
