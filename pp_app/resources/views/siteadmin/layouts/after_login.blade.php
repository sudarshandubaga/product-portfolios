@extends('siteadmin.layouts.app')

@section('body_content')
<div class="dashboard-body">
    <div class="dash-sidebar">
        <div class="sidebar-header">
            <img src="{{ url('handmade/images/logo.png') }}" alt="{{ $site->title }} Logo" title="{{ $site->title }} Logo">
            <span class="toggable">{{ $site->title }}</span>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard"></a>
        </div>
        <ul>
            <li class="current">
                <a href="{{ route('admin.dashboard') }}" title="Dashboard">
                    <i class="icon-dashboard1"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-images"></i>
                    <span class="nav-text">Sliders</span>
                </a>
                <ul>
                    <li>
                        <a href="">Sliders</a>
                    </li>
                    <li>
                        <a href="">Create New</a>
                    </li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-tag"></i> 
                    <span class="nav-text">Posts</span>
                </a>
                <ul>
                    <li>
                        <a href="">Posts</a>
                    </li>
                    <li>
                        <a href="">Create New</a>
                    </li>
                    <li>
                        <a href="">Categories</a>
                    </li>
                    <li>
                        <a href="">Comments</a>
                    </li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-file-text2"></i>
                    <span class="nav-text">Pages</span>
                </a>
                <ul>
                    <li>
                        <a href="">Pages</a>
                    </li>
                    <li>
                        <a href="">Create New</a>
                    </li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-shopping-bag"></i>
                    <span class="nav-text">Products</span>
                </a>
                <ul>
                    <li>
                        <a href="">Products</a>
                    </li>
                    <li>
                        <a href="">Create New</a>
                    </li>
                    <li>
                        <a href="">Categories</a>
                    </li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-cart2"></i>
                    <span class="nav-text">Orders <i class="badge">02</i></span>
                </a>
                <ul>
                    <li>
                        <a href="">Orders</a>
                    </li>
                    <li>
                        <a href="">Create New</a>
                    </li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-users"></i>
                    <span class="nav-text">Users</span>
                </a>
                <ul>
                    <li>
                        <a href="">Users</a>
                    </li>
                    <li>
                        <a href="">Create New</a>
                    </li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-settings1"></i>
                    <span class="nav-text">Settings</span>
                </a>
                <ul>
                    <li>
                        <a href="">Appearance</a>
                    </li>
                    <li>
                        <a href="">General Settings</a>
                    </li>
                    <li>
                        <a href="">Menu Settings</a>
                    </li>
                    <li>
                        <a href="">Email Settings</a>
                    </li>
                    <li>
                        <a href="">Image Settings</a>
                    </li>
                    <li>
                        <a href="">Form Settings</a>
                    </li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="">
                    <i class="icon-user"></i>
                    <span class="nav-text">Profile</span>
                </a>
                <ul>
                    <li>
                        <a href="">Edit Profile</a>
                    </li>
                    <li>
                        <a href="">Change Password</a>
                    </li>
                    <li>
                        <a href="">Logout</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="dash-right">
        <div class="right-header">
            <button type="button" class="nav-bars">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div>
                <button type="button">
                    <i class="icon-bell"></i>
                </button>
                <button type="button">
                    <i class="icon-user"></i> Hi, {{ auth()->user()->name }}
                </button>
            </div>
        </div>
        @yield('content')
    </div>
</div>
@endsection