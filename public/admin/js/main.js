$(function () {
    $(window).on('load', function () {
        let hideNav = localStorage.getItem('hide-nav');
        if(hideNav) {
            $('.nav-bars').trigger('click');
        }
    });

    $(document).on('click', '.nav-bars', function (e) {
        e.preventDefault();

        $('body').toggleClass('hide-nav');
        
        if($('body').hasClass('hide-nav')) {
            $('.dash-sidebar').find('.toggable, .nav-text').hide();
            localStorage.setItem('hide-nav', 1);
        } else {
            localStorage.removeItem('hide-nav')
            setTimeout(() => {
                $('.dash-sidebar').find('.toggable, .nav-text').show();  
            }, 500);
        }
    });

    $(document).on('click', 'body:not(.hide-nav) .dash-sidebar>ul>li.has-dropdown>a', function (e) {
        e.preventDefault();

        let parent = $(this).closest('li');
        parent.toggleClass('shown');
        parent.children('ul').slideToggle();

        $('.dash-sidebar>ul>li.has-dropdown>a').not(this).closest('li').find('ul').slideUp();
        $('.dash-sidebar>ul>li.has-dropdown>a').not(this).closest('li').removeClass('shown');
    });
});