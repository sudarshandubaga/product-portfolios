<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') | {{ $site->title }}</title>

    <!-- Admin CSS -->
    <link rel="stylesheet" href="{{ url('admin/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('icomoon/style.css') }}">
</head>
<body>
    @yield('body_content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>
    <script src="{{ url('admin/js/main.js') }}" type="module" defer></script>

    @yield('extra_scripts')
</body>
</html>