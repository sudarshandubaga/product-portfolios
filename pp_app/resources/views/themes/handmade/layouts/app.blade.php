<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320">
    <!-- favicon links-->
    <link rel="icon" type="image/icon" href="favicon.png" />
    <!-- main css -->
    <link rel="stylesheet" href="{{ url('handmade/css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ url('handmade/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ url('handmade/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ url('handmade/css/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ url('handmade/css/custom_animation.css') }}" />
    <link rel="stylesheet" href="{{ url('handmade/js/plugin/owl/owl.carousel.css') }}" />
    <!--revolution-->
    <link href="{{ url('handmade/js/plugin/revolution/css/settings.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('handmade/js/plugin/revolution/css/layers.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('handmade/js/plugin/revolution/css/navigation.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ url('handmade/css/style.css') }}" media="screen" />
    <link rel="stylesheet" href="{{ url('handmade/css/colors/default.css') }}" id="colors" />
</head>

<body>
    <!--preloader-->
    <div class="loader">
        <div class="loader-inner ball-scale-ripple-multiple">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    
    @yield('body_content')
    
    <!--script-->
    <script type="text/javascript" src="{{ url('handmade/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ url('handmade/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ url('handmade/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('handmade/js/plugin/owl/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('handmade/js/jquery.magnific-popup.js') }}"></script>
    
    <!--Revolution slider js-->
    <script src="{{ url('handmade/js/plugin/revolution/js/jquery.themepunch.revolution.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('handmade/js/plugin/revolution/js/jquery.themepunch.tools.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('handmade/js/plugin/revolution/js/revolution.extension.navigation.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('handmade/js/plugin/revolution/js/revolution.extension.slideanims.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('handmade/js/plugin/revolution/js/revolution.extension.layeranimation.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('handmade/js/plugin/revolution/js/revolution.extension.parallax.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('handmade/js/plugin/revolution/js/revolution.extension.kenburn.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('handmade/js/switcher.js') }}"></script>
    <script type="text/javascript" src="{{ url('handmade/js/custom.js') }}"></script>
</body>
</html>