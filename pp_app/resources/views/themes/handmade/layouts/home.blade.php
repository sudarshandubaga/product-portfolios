@extends('themes.handmade.layouts.app')

@section('body_content')
<div id="wrapper">
    <!--top_header start-->
    <div class="hm_home_style1 hm_home_style3">
        <!--menu start-->
        <div class="hm_menu_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="hm_logo">
                            <a href="index-2.html"><img src="{{ url('handmade/images/logo.png') }}" alt="handmade-craft-logo" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="hm_menu_wrap">
                            <nav>
                                <div class="menu_btn"><span></span><span></span><span></span></div>
                                <div class="hm_menu">
                                    <ul>
                                        <li class="active dropdown"><a href="index-2.html">home</a>
                                        <ul class="sub-menu">
                                            <li><a href="home-style1.html">home style1</a></li>
                                            <li><a href="home-style2.html">home style2</a></li>
                                            <li><a href="home-style3.html">home style3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about.html">about</a></li>
                                    <li><a href="services.html">services</a></li>
                                    <li class="dropdown"><a href="javacsript:;">gallery</a>
                                    <ul class="sub-menu">
                                        <li><a href="gallery-3-column.html">gallery 3 column</a></li>
                                        <li><a href="gallery-4-column.html">gallery 4 column</a></li>
                                        <li><a href="gallery-fullwidth.html">gallery full width</a></li>
                                        <li><a href="gallery.html">gallery masonry</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="javascript:;">blog</a>
                                <ul class="sub-menu">
                                    <li><a href="blog-two-column.html">blog two column</a></li>
                                    <li><a href="blog-three-column.html">blog three column</a></li>
                                    <li><a href="blog-single.html">blog single</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="javascript:;">shop</a>
                            <ul class="sub-menu">
                                <li><a href="shop.html">shop</a></li>
                                <li><a href="shop-single.html">shop single</a></li>
                            </ul>
                        </li>
                        <li><a href="contact.html">contact</a></li>
                    </ul>
                </div>
            </nav>
            <div class="hm_header_search">
                <i class="fa fa-search" aria-hidden="true"></i>
                <div class="hm_search_box">
                    <div class="icon_close">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </div>
                    <form>
                        <input type="search" name="search" placeholder="Search here...">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@yield('content')
<!--footer start-->
<div class="hm_footer_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="hm_footer_box">
                    <img src="{{ url('handmade/images/logo.png') }}" alt="handmade-craft-logo" class="img-responsive">
                    <p class="footer_about_para">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="hm_footer_box">
                    <h3 class="footer_heading">Help</h3>
                    <ul>
                        <li><a href="#">Find Your Beer</a></li>
                        <li><a href="#">Customer Service</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Recent Orders</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="hm_footer_box">
                    <h3 class="footer_heading">links</h3>
                    <ul>
                        <li><a href="index-2.html">home</a></li>
                        <li><a href="shop.html">shop</a></li>
                        <li><a href="404.html">404 page</a></li>
                        <li><a href="javascript:;">login</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="hm_footer_box hm_footer_contact">
                    <h3 class="footer_heading">Our Information</h3>
                    <ul>
                        <li><span class="footer_icon"><i class="fa fa-map-marker" aria-hidden="true"></i> </span>
                        <p class="foo_con_para">787 Lakeview St. Marion, NC 28752</p>
                    </li>
                    <li><span class="footer_icon"><i class="fa fa-phone" aria-hidden="true"></i> </span>
                    <p class="foo_con_para">+1800123654789 +1800123456788</p>
                </li>
                <li><a href="mailto:jhini.mehta@gmail.com"><span class="footer_icon"><i class="fa fa-envelope" aria-hidden="true"></i> </span><p class="foo_con_para">support@handmade.net</p></a></li>
            </ul>
        </div>
    </div>
</div>
</div>
</div>
<!--bottom footer start-->
<div class="hm_bottom_footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <p>Copyrights &copy; 2018 All Rights Reserved by Himanshusofttech</p>
            </div>
            <div class="col-lg-6 col-md-6">
                <ul class="footer_so_icons">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--main wrapper-->
</div>
@endsection