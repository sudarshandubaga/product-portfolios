<?php

namespace App\Http\Controllers;

use App\Models\SiteProduct;
use Illuminate\Http\Request;

class SiteProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteProduct  $siteProduct
     * @return \Illuminate\Http\Response
     */
    public function show(SiteProduct $siteProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteProduct  $siteProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteProduct $siteProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteProduct  $siteProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteProduct $siteProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteProduct  $siteProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteProduct $siteProduct)
    {
        //
    }
}
