<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('siteadmin.auth.login');
    }

    public function authorise(Request $request)
    {
        $request->validate([
            'login'     => 'required',
            'password'  => 'required'
        ]);

        $auth_data = $request->only(['login', 'password']);
        $auth_data['site_id'] = $this->_site->id;

        $admin = auth()->guard('admin')->attempt($auth_data);

        // dd($admin);

        if($admin)
        {
            return redirect()->route('admin.dashboard');
        }
        else
        {
            return redirect()->back()->with('alert-danger', 'Wrong Data! Credentials not matched.');
        }
    }
}
