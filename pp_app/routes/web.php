<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['domain' => env('DOMAIN'), 'namespace' => 'App\Http\Controllers\MasterWeb'], function () {
    Route::get('/', 'HomeController@index');
});


Route::group(['namespace' => 'App\Http\Controllers\Web'], function () {
    Route::get('/', 'HomeController@index');
});

Route::group(['prefix' => 'pp-admin', 'as' => 'admin.', 'namespace' => 'App\Http\Controllers'], function () {
    Route::group(['middleware' => 'guest:admin'], function () {
        Route::get('/login', 'AuthController@login')->name('login');
        Route::post('/login', 'AuthController@authorise')->name('login.post');
    });

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
    });
});