<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{url('master/css/style.css')}}">
    <link rel="stylesheet" href="{{url('icomoon/style.css')}}">
</head>
<body>
    <section class="vh-100 welcome-section">
        <div class="bg-primary welcome-content">
            <nav>
                <ul>
                    <li>
                        <a href="#home">Home</a>
                    </li>
                    <li>
                        <a href="#about">About</a>
                    </li>
                    <li>
                        <a href="#pricing">Pricing</a>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </nav>

            <div class="welcome-slogan">
                <img src="{{ url('master/images/portfolios.png') }}" alt="">
                <p>Grow your business by making your own website with us.</p>
                <h1>
                    We Build Porfolios Website At Cheapest Cost
                </h1>
                <button class="btn-white-outline rounded">Get Started <i class="icon-angle-right"></i></button>
            </div>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" class="section-after-svg">
            <path fill-opacity="1" d="M0,0L48,10.7C96,21,192,43,288,80C384,117,480,171,576,176C672,181,768,139,864,128C960,117,1056,139,1152,160C1248,181,1344,203,1392,213.3L1440,224L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path>
        </svg>
    </section>
    <section class="vh-100">
        <div class="container">
            fasdfkljk
        </div>
    </section>
</body>
</html>