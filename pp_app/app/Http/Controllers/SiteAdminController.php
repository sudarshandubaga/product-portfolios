<?php

namespace App\Http\Controllers;

use App\Models\SiteAdmin;
use Illuminate\Http\Request;

class SiteAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteAdmin  $siteAdmin
     * @return \Illuminate\Http\Response
     */
    public function show(SiteAdmin $siteAdmin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteAdmin  $siteAdmin
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteAdmin $siteAdmin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteAdmin  $siteAdmin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteAdmin $siteAdmin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteAdmin  $siteAdmin
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteAdmin $siteAdmin)
    {
        //
    }
}
