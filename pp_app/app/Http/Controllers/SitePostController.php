<?php

namespace App\Http\Controllers;

use App\Models\SitePost;
use Illuminate\Http\Request;

class SitePostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SitePost  $sitePost
     * @return \Illuminate\Http\Response
     */
    public function show(SitePost $sitePost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SitePost  $sitePost
     * @return \Illuminate\Http\Response
     */
    public function edit(SitePost $sitePost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SitePost  $sitePost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SitePost $sitePost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SitePost  $sitePost
     * @return \Illuminate\Http\Response
     */
    public function destroy(SitePost $sitePost)
    {
        //
    }
}
