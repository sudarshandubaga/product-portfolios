<?php

namespace App\Http\Controllers;

use App\Models\SiteCategory;
use Illuminate\Http\Request;

class SiteCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteCategory  $siteCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SiteCategory $siteCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteCategory  $siteCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteCategory $siteCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteCategory  $siteCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteCategory $siteCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteCategory  $siteCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteCategory $siteCategory)
    {
        //
    }
}
