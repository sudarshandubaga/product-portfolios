<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Site;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $_site = null;

    public function __construct()
    {
        $domain = request()->getHost();

        if($domain !== env('DOMAIN')) {

            $site = $this->_site = Site::where('domain', $domain)->firstOrFail();
    
            \View::share('site', $site);
        }
        
    }

    public function loadView($view)
    {
        $theme = 'handmade';
        return view("themes.{$theme}.".$view);
    }
}
