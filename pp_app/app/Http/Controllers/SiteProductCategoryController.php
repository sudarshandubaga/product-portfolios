<?php

namespace App\Http\Controllers;

use App\Models\SiteProductCategory;
use Illuminate\Http\Request;

class SiteProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteProductCategory  $siteProductCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SiteProductCategory $siteProductCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteProductCategory  $siteProductCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteProductCategory $siteProductCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiteProductCategory  $siteProductCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteProductCategory $siteProductCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteProductCategory  $siteProductCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteProductCategory $siteProductCategory)
    {
        //
    }
}
