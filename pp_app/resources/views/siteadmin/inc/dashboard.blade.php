@extends('siteadmin.layouts.after_login')

@section('title', 'Dashboard')

@section('content')

<div class="container-fluid">
    <div class="row mt-15">
        <div class="col-66">
            <div class="card">
                <div class="card-body">
                    <canvas id="orderSummaryChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-33">
            <div class="card">
                <div class="card-body">
                    <canvas id="comparasionChart"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('extra_scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js" defer></script>
<script src="{{ url('admin/js/chunks/dashboard.js') }}" defer></script>
@endsection