<?php

namespace App\Http\Controllers\MasterWeb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('master.inc.homepage');
    }
}
